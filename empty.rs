use std::fs::File;
use std::env;

fn fcheck(fname: &str) -> std::io::Result<()> {
    let _file = File::open(fname)?;
    Ok(())
}

fn ftrunc(fname: &str) -> std::io::Result<()> {
    let mut _file = File::create(fname);
    Ok(())
}

fn main() {
    let mut i = 0;
    for arg in env::args() {
        if i == 0{
            i+=1;
            continue;
        }
        let mut res = fcheck(&arg);
        if res.is_ok() {
            println!("{} is a readable file", &arg);
            res = ftrunc(&arg);
            if res.is_ok() {
                println!("emptied {}", &arg);
            } else {
                println!("could not empty {}", &arg);
            }
        } else {
            println!("{} is not a readable file", &arg);
        }
    }
}
